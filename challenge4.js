function landMass(country, mass) {
  //Implementación

  const hundred = 148940000; // 100%
  const percent = parseFloat(((mass * 100) / hundred).toFixed(2));

  return {
    percent: percent,
    message: `${country} representa el ${percent}% de la masa de la tierra`,
  };
}

module.exports = landMass;
